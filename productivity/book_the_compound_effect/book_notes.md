# Introduction

Mastering a topic such as in this case software engineering is not possible without hard work, a sound strategy, and the right attitude.

Employing the strategies and thinking from books such as The Compound Effect by Darren Hardy can possibly be helpful. This document holds notes and action points to be taken from the book.

## Chapter 1 Summary Action Steps

Excuses that I cling that can put me down and I need to work on is for example the following. Can think that I started to late in life to really focus on software engineering but there is in reality many years left in my career and everyone in the business need to evolve and learn new things all the time anyway. Another sort of excuse for not doing something can be that I feel that do not have the right equipment, such as debuggers and good enough computer, and need to wait and get this first until I can even start building my next project. This is also a bad excuse that I need to learn to ignore and if equipment really is a problem so should this be prioritized over other things that I buy.

Smalls steps that will compound to great progress that I should take:
  - Watch less movies, news, and series. Listen to programming podcasts or work on an online course instead.
  - Eat healthier, varied food that is good for the brain. And drink more water and less soda.
  - Continue exercising a lot but limit the time spent on this this by running more intense shorter workouts that have the same effect frees up time that can be spent coding.
  - Try not to repeat learning things I already know and challenge myself with new ore more advanced instead.
  - Get involved in a community or open source project.
  - Pay for courses and training since this helps to stay motivated.
  - Do not snooze and always go up early.
  - Delegate or pay people to do service for me not related to software engineering.
  - Reduce mail subscriptions to spend less time on irrelevant email.
  - Meditate each morning to get in a good mode and get focused.
  - Have relevant prioritized goals for the day.
  - Evaluate the day at the end of the day.

Small actions to be avoided:
  - Stay away from negative people and naysayers.
  - Try to limit other interests that steals focus such as gaming, gardening, home improvement, shopping, cooking, etc.
  - No alcohol, sugar or other drugs with the exception for some coffee maybe.
  - No plan less web browsing or do this on scheduled time limited.
  - Stay away from sites with much advertising that I tend to click on and lose focus and time.

Areas where I before Have been successful but need to not take for granted is among others mathematics and C++. Easy to get rusty on mathematics when not using it, and C++ have evolved a lot the last 10 years.

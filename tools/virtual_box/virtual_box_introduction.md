# Introduction to VirtualBox

VirtualBox is a virtual machine monitor for creating and running of virtual machines. What this means is that VirtualBox can be used to manage more than one operating system on a single computer. This is done by emulating an entire computer on the real computer.

https://www.virtualbox.org/wiki/Downloads

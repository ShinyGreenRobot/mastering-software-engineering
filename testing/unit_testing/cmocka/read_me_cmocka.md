# cmocka

cmocka is a unit testing framework for C code projects.

## References

[cmocka website](https://cmocka.org/)  
[Unit testing embedded code with cmocka, Sam Lewis](http://www.samlewis.me/2016/09/embedded-unit-testing-with-cmocka/)

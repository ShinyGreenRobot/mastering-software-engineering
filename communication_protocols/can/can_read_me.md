# Controller Area Network

Controller Area Network (CAN) is a communication standard for exchange of data between  microcontrollers.

## References

<https://elearning.vector.com/mod/page/view.php?id=333>
<https://www.can-cia.org/can-knowledge/can/can-history/>
<https://en.wikipedia.org/wiki/CAN_bus>
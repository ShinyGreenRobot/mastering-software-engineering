# Quiz Chapter 1

1.  What are the six main reasons that people choose to use Python?

    1.  Readable and maintainable code.
    2.  Supports multiple programming paradigms.
    3.  Compatible with many platforms and systems.
    4.  Robust standard library.
    5.  Active community
    6.  Productivity


2.  Name four notable companies or organizations using Python today.

    1.  Google.
    2.  Netflix.
    3.  Spotify.
    4.  NASA.


3.  Why might you not want to use Python in an application?

    -   Programs written in some other languages will run much faster, examples are Assembler, C, and C++. So Python can be a bad choice for computational intensive applications.
    - Python is not suited for low cost embedded systems because of too high requirements on the hardware needed to be able to run Python.
    -  Python cannot be used for safety critical real time system because there are no guarantees on timings of the dynamic memory handling.
    -  Python is dynamically typed meaning that some bugs will not be discovered in the early stage and will instead hopefully be noted when testing at run time. This means that Python will require more testing than a statically typed language that will have a compiler that will point out the errors directly.


4.  What can you do with Python?

    - Web development
    - Data science, including machine learning, data analysis, and data visualization.
    - Scripting
    - GUI development.
    - Embedded systems prototyping.


5.  What is the significance of the Python `import this` statement?

    This is an Easter egg and execution of the statement will print out a set rules called The Zen of Python, that tries to capture the philosophy of Python.


6.  Why does "spam" show up in so many Python examples in books and on the Web?

    The Python programming language is named after the comedy group called Monty Python and one sketch from this group is called "Spam".

7.  What is your favourite colour?

    This is not a real question and instead just a reference to a Monty Python sketch called "Bridge of Death", see previous question and answer for more information.

# Polyspace

Polyspace is a group of static code analysis products that use formal methods to prove the absence of run-time errors.

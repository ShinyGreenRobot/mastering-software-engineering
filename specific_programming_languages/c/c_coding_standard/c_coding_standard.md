# C Coding Standard - Shiny Green Robot Version

## Formatting

## References

<https://users.ece.cmu.edu/~eno/coding/CCodingStandard.html#names>

<https://www.kernel.org/doc/html/v4.10/process/coding-style.html>

<https://developer.gnome.org/programming-guidelines/stable/c-coding-style.html.en>

<https://cs.brown.edu/courses/csci0330/docs/guides/style.pdf>

<https://openresty.org/en/c-coding-style-guide.html>

<https://wiki.sei.cmu.edu/confluence/display/c/SEI+CERT+C+Coding+Standar>

<https://pep8.org/>

<https://barrgroup.com/Embedded-Systems/Books/Embedded-C-Coding-Standard>
